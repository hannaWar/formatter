#include <ncurses.h>
#include "ItemsSelector.h"

const string &ItemsSelector::getLastSelection() {
    return lastSelection;
}

string ItemsSelector::select(const vector<string> &data) {

    int choice = 0;

    initscr();
    curs_set(0);//убиваем прямоугольничек с курсором
    keypad(stdscr, TRUE);

    for (auto &item : data) //Проходим по всем элементам меню
    {
        printw("  %s\n\r", item.c_str());
    }

    bool moving = true;
    while (moving) {

        for (int i = 0; i < data.size(); ++i) {
            if (i == choice) {
                mvaddch(i, 0, L'>');
            } else {
                mvaddch(i, 0, ' ');
            }
        }
//
        //Получаем нажатие пользователя
        int key = getch();
        switch (key) {
            case KEY_UP:
                if (choice) //Если возможно, переводим указатель вверх
                    choice--;

                break;
            case KEY_DOWN:
                if (choice < data.size() - 1) //Если возможно, переводим указатель вниз
                    choice++;
                break;
            case 10:
                moving = false;
                break;
            default:
                printw("%d\r\n", key);
                break;
        }
    }
    clear();
    endwin();

    lastSelection = data[choice];

    return lastSelection;

}
