#ifndef FORMATTER_FORMATTER_H
#define FORMATTER_FORMATTER_H

#include <string>

using std::string;

class Formatter {

public:

    virtual void format(string device) = 0;

};


#endif //FORMATTER_FORMATTER_H
