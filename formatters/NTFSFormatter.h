#ifndef FORMATTER_NTFSFORMATTER_H
#define FORMATTER_NTFSFORMATTER_H


#include "formatter.h"

class NTFSFormatter : public Formatter {

public:
    void format(string device) override;

};


#endif //FORMATTER_NTFSFORMATTER_H
