#ifndef FORMATTER_FAT32FORMATTER_H
#define FORMATTER_FAT32FORMATTER_H


#include "formatter.h"

class Fat32Formatter : public Formatter {

public:
    void format(string device) override;

};


#endif //FORMATTER_FAT32FORMATTER_H
