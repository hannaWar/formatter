#ifndef FORMATTER_EXT3FORMATTER_H
#define FORMATTER_EXT3FORMATTER_H


#include "formatter.h"

class Ext3Formatter : public Formatter {

public:
    void format(string device) override;

};


#endif //FORMATTER_EXT3FORMATTER_H
