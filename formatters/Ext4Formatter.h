#ifndef FORMATTER_EXT4FORMATTER_H
#define FORMATTER_EXT4FORMATTER_H

#include "formatter.h"

class Ext4Formatter : public Formatter {

public:

    void format(string device) override;

};


#endif //FORMATTER_EXT4FORMATTER_H
