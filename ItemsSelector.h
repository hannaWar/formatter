#ifndef FORMATTER_ITEMSSELECTOR_H
#define FORMATTER_ITEMSSELECTOR_H

#include <string>
#include <vector>

using std::vector;
using std::string;


class ItemsSelector {

    string lastSelection;

public:

    const string &getLastSelection();

    string select(const vector<string> &data);

};


#endif //FORMATTER_ITEMSSELECTOR_H
