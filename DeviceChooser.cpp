#include "DeviceChooser.h"

DeviceChooser::DeviceChooser() {
    allDevices = vector<string>();
    filteredDevices = set<string>();

    DIR *dir = opendir("/dev/");

    if (dir) {

        dirent *device;
        while ((device = readdir(dir)) != nullptr) {
            allDevices.emplace_back(device->d_name);
        }

    } else {
        exit(DEVICE_DIR_NOT_FOUND_ERROR);
    }

}

void DeviceChooser::addToFiltered(const string &exp) {
    for (const auto &device : allDevices) {
        if (device.find(exp) != string::npos) {
            filteredDevices.insert(device);
        }
    }
}

vector<string> DeviceChooser::getFiltered() {
    return vector<string>(filteredDevices.begin(), filteredDevices.end());
}
