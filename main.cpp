#include <vector>
#include "formatters/formatter.h"
#include "formatters/Ext4Formatter.h"

#include "DeviceChooser.h"
#include "ItemsSelector.h"
#include "formatters/NTFSFormatter.h"
#include "formatters/Ext3Formatter.h"
#include "formatters/Fat32Formatter.h"

int main() {
    DeviceChooser deviceChooser = DeviceChooser();

    deviceChooser.addToFiltered("sd");
    deviceChooser.addToFiltered("nvme");
    deviceChooser.addToFiltered("hd");

    auto devices = deviceChooser.getFiltered();

    ItemsSelector selector{};

    auto device = selector.select(devices);

    auto type = selector.select(vector<string>({"ntfs", "ext4", "ext3", "fat32"}));

    Formatter *formatter;

    do {

        if (type == "ntfs") {
            formatter = new NTFSFormatter();
            break;
        }
        if (type == "ext4") {
            formatter = new Ext4Formatter();
            break;
        }
        if (type == "ext3") {
            formatter = new Ext3Formatter();
            break;
        }
        if (type == "fat32") {
            formatter = new Fat32Formatter();
            break;
        }
        exit(UNKNOWN_FS);
    } while (false);

    formatter->format(device);

    /// создать канал
    /// переопределить вывыод
    /// вызвать ls
    /// вернуть вывод

    return 0;
}

