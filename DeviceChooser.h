#ifndef FORMATTER_DEVICECHOOSER_H
#define FORMATTER_DEVICECHOOSER_H

#include <vector>
#include <set>
#include <string>
#include <dirent.h>
#include "constants.h"

using std::vector;
using std::string;
using std::set;

class DeviceChooser {

    vector<string> allDevices;
    set<string> filteredDevices;

public:

    DeviceChooser();

    void addToFiltered(const string &exp);

    vector<string> getFiltered();

};


#endif //FORMATTER_DEVICECHOOSER_H
